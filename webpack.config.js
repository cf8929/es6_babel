const path = require('path');
//匯入的語法require

//匯出的語法module.exports
module.exports = {
    entry: './src/app.js',
    mode: 'development', // development or production
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['babel-preset-env']
              }
            }
          }
        ]
      }
};