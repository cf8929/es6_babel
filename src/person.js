class Person { //ES6可以一個類別一個檔案，類別也是一個特殊的'物件'
    constructor(firstname, lastname, age = 20, gender = 'male') {//constructor()當作建構函式使用，其他地方不要用
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
    }

    toString() {
        return this.firstname + '' + this.lastname;
    }
    get fullname() {
        return this.firstname + '' + this.lastname;
    }

    set fullname(name){
        const ar = name.split(' ');
        // console.log('name: ' + name);
        this.firstname = ar[0];
        this.lastname = ar[1];
    }

    describe() {
        return `name: ${this.firstname} ${this.lastname}
age:${this.age} 
gender:${this.gender}`;
    }
}

export default Person;  //instance





