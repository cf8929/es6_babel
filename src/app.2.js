import Person from './person';

let app, divShow;
try {
    app = document.querySelector('#app');
    divShow = s => {
        app.innerHTML += s + '<br>';
    }
} catch(error) {
    divShow = console.log;
}


const p1 = new Person('Peter','Lee');
const p2 = new Person('Flora','Wu',26, 'female');

divShow(p1.describe());
divShow(p2.describe());

p1.birthday = '1990-10-20';
p1.toString = () =>{
    return '---';
};

divShow(p1.birthday);
divShow(p1.fullname);
divShow(p2.fullname);

p1.fullname = 'Bill Chen';
divShow(p1.fullname);
divShow(p1.describe());

