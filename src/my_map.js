const my_map = divShow => {
    var map = new Map();
    var obj = {};

    map.set({}, 12);
    map.set(obj, [3,4,5]);
    map.set(obj, [6,7,8]);

    divShow('size: ' + map.size);
    divShow(map.get(obj));
    divShow('---');
    map.forEach((v,k) => {
        divShow('key:'+JSON.stringify(k)+':::'+'value:'+JSON.stringify(v));
    });

    const set = new Set(); //不重複的集合
    const obj2 = {a:12};

    set.add([2,3]);
    set.add([5,3]).add(obj2);
    divShow('---');
    set.add(obj2); //重複加入參照還是參照
    set.add([2,3]);//新建一個陣列
    divShow(set.size);
    set.forEach((v,k) => {
        divShow('key:'+JSON.stringify(k)+':::'+'value:'+JSON.stringify(v));//add沒有設定兩個參數,key就會等於value
    });

};

export default my_map;