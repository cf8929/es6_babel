class Person {
    constructor(firstname, lastname, age = 20, gender = 'male') {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
    }

    toString() {
        return this.firstname + '' + this.lastname;
    }

    describe() {
        return `name: ${this.firstname} ${this.lastname}
age:${this.age} 
gender:${this.gender}`;
    }
}
let p1 = new Person('Peter','Lee');

console.log(p1.toString());
console.log(p1.describe());

let app = document.querySelector('#app');
app.innerHTML = p1.describe();