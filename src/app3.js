import Person from './person';
import ar from './a';

let p1 = new Person('Peter','Lee');

console.log(p1.toString());
console.log(p1.describe());

var bb=10;
let cc=20;
const dd=30;
{
    var bb = 100;
    let cc = 200;
    const dd = 300;

    console.log('inner: ',bb,cc,dd);
}

console.log('outer: ',bb, cc, dd);
//var沒有{區域性}，let const有{區域性}

let app = document.querySelector('#app');
app.innerHTML = p1.describe();
ar.push('hihi');
app.innerHTML += '<br>' + ar.toString();