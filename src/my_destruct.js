const my_destruct = divShow =>{
    const person = {
        name: 'bill',
        age: 28,
        gender: 'male'
    }

    let {name, age, gender} = person;
    let {a, b, c} = person;

    divShow(name);
    divShow(age);//與順序不相關，還是可以得到值
    divShow(gender);
    divShow(a+' '+b+' '+c);

    const ar = [6,9,12];
    let [aa,bb,cc] = ar;
    divShow(aa+' '+bb+' '+cc);

    const ar2 = [
        ...ar, //...複製陣列出來，只能放在陣列或參數列
        20
    ];
    divShow(ar2.toString());
};

export default my_destruct;