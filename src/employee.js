import Person from './person';

class Employee extends Person {
    constructor(firstname, lastname){
        super(firstname, lastname); //只能在constructor裡用，繼承父類別Person
    }
}

export default Employee;