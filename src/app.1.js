import my_fetch from './my_fetch';

let app, divShow;
try {
    app = document.querySelector('#app');
    divShow = s => {
        app.innerHTML += s + '<br>';
    }
} catch(error) {
    divShow = console.log;
}

my_fetch(divShow);