const my_obj = divShow =>{

    let [a,b,c] = [6,7,'hello'];
    const obj = {a,b,c};
    divShow(JSON.stringify(obj));

    // ES5 作法
    let tools = {
        func: function() {

        },
        func2: function(){

        }
    };
    //tools.func();



    // ES6 做法1
    let tools1= {
        func(){ //1.省略function()

        }
    };

    // ES6 做法2
    let tools2 = {
        func: () =>{ //2.箭頭寫法

        },
        func2: n=>{

        },

        }
    }


export default my_obj;