import Employee from './employee';

let app, divShow;
try {
    app = document.querySelector('#app');
    divShow = s => {
        app.innerHTML += s + '<br>';
    }
} catch(error) {
    divShow = console.log;
}


const p1 = new Employee('Aaron','Lin');

divShow(p1.fullname);
