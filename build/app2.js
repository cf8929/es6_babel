'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Person = function () {
    function Person(firstname, lastname) {
        var age = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 20;
        var gender = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'male';

        _classCallCheck(this, Person);

        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.gender = gender;
    }

    _createClass(Person, [{
        key: 'toString',
        value: function toString() {
            return this.firstname + '' + this.lastname;
        }
    }, {
        key: 'describe',
        value: function describe() {
            return 'name: ' + this.firstname + ' ' + this.lastname + '\nage:' + this.age + ' \ngender:' + this.gender;
        }
    }]);

    return Person;
}();

var p1 = new Person('Peter', 'Lee');

console.log(p1.toString());
console.log(p1.describe());

var app = document.querySelector('#app');
app.innerHTML = p1.describe();